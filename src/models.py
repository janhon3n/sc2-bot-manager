from .db import db


def to_dict(model_object):
  d = {}
  for column in model_object.__table__.columns:
    if column.name.startswith('_'):
      continue
    d[column.name] = str(getattr(model_object, column.name))
  return d


game_group_tag = db.Table(
    'game_group_tag',
    db.Column(
        'game_group_id',
        db.Integer,
        db.ForeignKey('game_group.id'),
        primary_key=True
    ),
    db.Column(
        'tag_id',
        db.Integer,
        db.ForeignKey('tag.id'),
        primary_key=True
    )
)

game_tag = db.Table(
    'game_tag',
    db.Column(
        'game_id',
        db.Integer,
        db.ForeignKey('game.id'),
        primary_key=True
    ),
    db.Column(
        'tag_id',
        db.Integer,
        db.ForeignKey('tag.id'),
        primary_key=True
    )
)


class Tag(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  tag = db.Column(db.String, unique=True)

  game_groups = db.relationship(
      'GameGroup', secondary=game_group_tag,
  )
  games = db.relationship(
      'Game', secondary=game_tag,
  )


class GameStep(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  game_id = db.Column(db.Integer, db.ForeignKey('game.id'))
  time = db.Column(db.Integer)
  score = db.Column(db.Integer)
  supply_used = db.Column(db.Integer)


class Game(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  game_group_id = db.Column(db.Integer, db.ForeignKey('game_group.id'))
  game_end_result = db.Column(db.String)
  steps = db.relationship('GameStep')

  tags = db.relationship(
      'Tag', secondary=game_tag,
  )


class GameGroup(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String)
  games = db.relationship('Game')

  tags = db.relationship(
      'Tag', secondary=game_group_tag,
  )
