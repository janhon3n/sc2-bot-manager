
from . import api_types
from . import models
import src.db
db = src.db.db


def create_game(api_game: api_types.Game):
  game = models.Game()
  for attr in ['game_group_id', 'game_end_result']:
    setattr(game, attr, getattr(api_types, attr))
  game.steps = []
  for s in api_game.steps:
    step = models.GameStep()
    for attr in ['time', 'score', 'supply_used']:
      setattr(game, attr, getattr(api_types, attr))
    step.game_id = game.id

  db.session.add_all(game.steps)
  db.session.add(game)
  db.session.commit()


def create_game_group(api_game_group: api_types.GameGroup):
  game_group = models.GameGroup(name=api_game_group.name)
  db.session.add(game_group)

  for t in api_game_group.tags:
    # Use existing or create new
    tag = db.session.query(models.Tag).filter_by(tag=t).first()
    if not tag:
      tag = models.Tag(tag=t)
      db.session.add(tag)
    game_group.tags.append(tag)
  db.session.commit()


def get_game_group(id: str):
  return models.to_dict(models.GameGroup.query.get(id))
