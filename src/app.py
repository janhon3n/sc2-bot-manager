
import yaml
from flask import Flask, request
from dacite import from_dict
from settings import get_config
from .db import db
from . import api_types
from . import features

app = Flask(__name__)
app.config.from_object(get_config())
db.init_app(app)


def respond(dict):
  return yaml.dump(dict)


@app.before_request
def parse_body():
  request.body = yaml.load(request.data, Loader=yaml.SafeLoader)


@app.route('/')
def front_page():
  return 'Welcome!'


@app.route('/game_group/<id>', methods=['GET'])
def get_game_group(id: str):
  game_group = features.get_game_group(id)
  return respond(game_group)


@app.route('/game_group', methods=['POST'])
def post_game_group():
  game_group: api_types.GameGroup = from_dict(
      data_class=api_types.GameGroup, data=request.body
  )
  api_types.GameGroupValidator.validate(game_group)
  features.create_game_group(game_group)
  return respond({"status": "success"})


@app.route('/game', methods=['GET', 'POST'])
def game():
  game_group: api_types.Game = from_dict(
      data_class=api_types.Game, data=request.get_json()
  )
  features.create_game(game_group)
  return respond({"status": "success"})


@app.errorhandler(AssertionError,)
def page_not_found(e):
  return respond({"status": "error", "message": str(e)}), 400


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8765)
