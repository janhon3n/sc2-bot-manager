from dataclasses import dataclass
from typing import List, Optional


@dataclass
class GameStep:
  time: int
  score: int
  supply_used: int


@dataclass
class Game:
  steps: List[GameStep]
  game_end_result: str
  game_group_id: int


@dataclass
class GameGroup:
  name: str
  tags: List[str]
  games: Optional[List[int]]


class GameValidator:
  @staticmethod
  def validate(game: Game):
    assert game.game_group_id is not None


class GameGroupValidator:
  @staticmethod
  def validate(game_group: GameGroup):
    assert game_group.name is not None, "Name must be set"
    assert game_group.name != "", "Name cannot be empty"
