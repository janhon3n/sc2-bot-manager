from src.app import app
from src.db import db
import pytest


@pytest.fixture(scope="session", autouse=True)
def setup_db():
  context = app.app_context()
  context.__enter__()
  db.create_all()
  yield None
  context.__exit__(None, None, None)
