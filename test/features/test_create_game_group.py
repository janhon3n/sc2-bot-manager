import pytest
from src import api_types, features, models


def test():
  features.create_game_group(
      api_types.GameGroup(
          name="Test group",
          tags=["test tag 1", "test tag 2"],
          games=None
      )
  )
  assert models.GameGroup.query.count() == 1
  assert models.Tag.query.count() == 2


def test_name_cannot_be_empty():
  with pytest.raises(Exception):
    features.create_game_group(
        api_types.GameGroup(name="", tags=[], games=None)
    )
